package sbu.cs;

public class BlueBlock extends Block{
    public String vOutPut, hOutPut;
    public BlueBlock(String hInPut, String vInPut, int operator){
        super();
        switch (operator) {
            case 1:
                vOutPut = blackFunction1(vInPut);
                hOutPut = blackFunction1(hInPut);
                break;
            case 2:
                vOutPut = blackFunction2(vInPut);
                hOutPut = blackFunction2(hInPut);
                break;
            case 3:
                vOutPut = blackFunction3(vInPut);
                hOutPut = blackFunction3(hInPut);
                break;
            case 4:
                vOutPut = blackFunction4(vInPut);
                hOutPut = blackFunction4(hInPut);
                break;
            case 5:
                vOutPut = blackFunction5(vInPut);
                hOutPut = blackFunction5(hInPut);
                break;
        }
    }

    public String getvOutPut() {
        return vOutPut;
    }

    public String gethOutPut() {
        return hOutPut;
    }
}
