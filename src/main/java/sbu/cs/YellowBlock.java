package sbu.cs;

public class YellowBlock extends Block{
    public String outPut, vOutPut, hOutPut;
    public YellowBlock (String input, int operator) {
        switch (operator) {
            case 1:
                outPut = blackFunction1(input);
                break;
            case 2:
                outPut = blackFunction2(input);
                break;
            case 3:
                outPut = blackFunction3(input);
                break;
            case 4:
                outPut = blackFunction4(input);
                break;
            case 5:
                outPut = blackFunction5(input);
                break;
        }
        vOutPut = outPut;
        hOutPut = vOutPut;
    }

    public String getOutPut() {
        return outPut;
    }

    @Override
    public String getvOutPut() {
        return vOutPut;
    }

    @Override
    public String gethOutPut() {
        return hOutPut;
    }
}
