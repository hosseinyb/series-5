package sbu.cs;

public class PinkBlock extends Block{
    public String outPut, vOutPut,hOutPut;
    public PinkBlock(String input1, String input2, int operator) {
        super();
        switch (operator) {
            case 1:
                outPut = whiteFunction1(input1,input2);
                break;
            case 2:
                outPut = whiteFunction2(input1,input2);
                break;
            case 3:
                outPut = whiteFunction3(input1,input2);
                break;
            case 4:
                outPut  = whiteFunction4(input1,input2);
                break;
             case 5:
                outPut  = whiteFunction5(input1,input2);
                break;
        }
        vOutPut = outPut;
        hOutPut = outPut;
    }

    public String getOutPut() {
        return outPut;
    }

    @Override
    public String getvOutPut() {
        return vOutPut;
    }

    @Override
    public String gethOutPut() {
        return hOutPut;
    }
}
