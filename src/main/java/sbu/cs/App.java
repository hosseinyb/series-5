package sbu.cs;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {
        Block[][] data = new Block[n][n];
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                //Initilizing first Block:
                if (i == 0 && j == 0)
                    data[i][j] = new GreenBlock(input, arr[i][j]);
                    //Initilize GreenBlocks:
                else if(i == 0 && j != n - 1)
                    data[i][j] = new GreenBlock(data[i][j - 1].gethOutPut(), arr[i][j]);
                else if(j == 0 && i != n - 1)
                    data[i][j] = new GreenBlock(data[i - 1][j].getvOutPut(), arr[i][j]);
                    //Initilize YellowBlocks:
                else if(i == 0)
                    data[i][j] = new YellowBlock(data[i][j - 1].gethOutPut(), arr[i][j]);
                else if(j == 0)
                    data[i][j] = new YellowBlock(data[i - 1][j].getvOutPut(), arr[i][j]);
                    //Initializing Blue:
                else if(i > 0 && i < n - 1 && j > 0 && j < n - 1)
                    data[i][j] = new BlueBlock(data[i][j - 1].gethOutPut(), data[i - 1][j].getvOutPut(),arr[i][j]);
                    //Initializing PinkBlocks:
                else if(i == n - 1) {
                    System.out.println(data[i - 1][j].getvOutPut() + " , " + data[i][j - 1].gethOutPut());
                    data[i][j] = new PinkBlock(data[i][j - 1].gethOutPut(), data[i - 1][j].getvOutPut(), arr[i][j]);
                }
                else if(j == n - 1) {
                    System.out.println(data[i - 1][j].getvOutPut() + " , " + data[i][j - 1].gethOutPut());
                    data[i][j] = new PinkBlock(data[i - 1][j].getvOutPut(), data[i][j - 1].gethOutPut(), arr[i][j]);
                }
            }
        }
        return data[n-1][n-1].getOutPut();
    }

}
