package sbu.cs;

public class GreenBlock extends Block{
    public String vOutPut, hOutPut,outPut;


    public GreenBlock(String input, int operator) {
        super();
        switch (operator) {
            case 1:
                vOutPut = blackFunction1(input);
                break;
            case 2:
                vOutPut = blackFunction2(input);
                break;
            case 3:
                vOutPut = blackFunction3(input);
                break;
            case 4:
                vOutPut = blackFunction4(input);
                break;
            case 5:
                vOutPut = blackFunction5(input);
                break;
        }
        hOutPut = vOutPut;
    }

    public String getvOutPut() {
        return vOutPut;
    }

    public String gethOutPut() {
        return hOutPut;
    }

    @Override
    public String getOutPut() {
        return outPut;
    }
}
