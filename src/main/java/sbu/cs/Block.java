package sbu.cs;

public class Block {
    public String vOutPut, hOutPut,outPut;

    //black functions
    public String blackFunction1 (String input) {
        String answer = "";
        for (int i = 0; i < input.length(); i++) {
            answer = input.charAt(i) + answer;
        }
        return answer;
    }

    public String blackFunction2 (String input) {
        String answer = "";
        for (int i = 0; i < input.length(); i++) {
            answer = answer + input.charAt(i) + input.charAt(i);
        }
        return answer;
    }

    public String blackFunction3 (String input) {
        String answer = input + input;
        return answer;
    }

    public String blackFunction4 (String input) {
        String answer = input.charAt(input.length() - 1) + input.substring(0,input.length() - 1);
        return answer;
    }

    public String blackFunction5 (String input) {
        String answer = "";
        char[] alphabet = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        for (int i = 0; i < input.length(); i++) {
            char charakter = alphabet[25 - findingIndexOfAlphabet(input.charAt(i))];
            answer = answer + charakter;
        }
        return answer;
    }

    private int findingIndexOfAlphabet(char c) {
        char[] alphabet = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        for (int i = 0; i < alphabet.length; i++) {
            if (c == alphabet[i]) {
                return i;
            }
        }
        return -1;
    }
    //white functions
    public String whiteFunction1 (String input1, String input2) {
        String answer = "";
        int i1 = 0, i2 = 0;
        while (i1 < input1.length() || i2 < input2.length()) {
            if (i1 < input1.length())
                answer += input1.charAt(i1++);
            if (i2 < input2.length())
                answer += input2.charAt(i2++);
        }
        return answer;
    }

    public String whiteFunction2 (String input1, String input2) {
        String answer = input1 + blackFunction1(input2);
        return answer;
    }

    public String whiteFunction3 (String input1, String input2) {
        String answer = "";
        input2 = blackFunction1(input2);
        answer = whiteFunction1(input1, input2);
        return answer;
    }

    public String whiteFunction4 (String input1, String input2) {
        if (input1.length() % 2 == 0) {
            return input1;
        }
        return input2;
    }

    public String whiteFunction5 (String input1, String input2) {
        String answer = "";
        char[] alphabet = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        int minCount, remain = 0;
        if (input1.length() > input2.length())
            minCount = input2.length();
        else
            minCount = input1.length();
        for (int i = 0; i < minCount; i++) {
            answer = answer + alphabet[(findingIndexOfAlphabet(input1.charAt(i)) + findingIndexOfAlphabet(input2.charAt(i))) % 26];
        }
        if (minCount == input1.length()) {
            answer = answer + input2.substring(minCount);
        }
        if (minCount == input2.length()) {
            answer = answer + input1.substring(minCount);
        }
        return answer;
    }

    public String getvOutPut() {
        return vOutPut;
    }

    public String gethOutPut() {
        return hOutPut;
    }

    public String getOutPut() {
        return outPut;
    }
}
