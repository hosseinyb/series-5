package sbu.cs;

import java.util.regex.Pattern;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        for (int i = 0; i < size -1; i++) {
            int minimumIndex = i;
            for (int j = i; j < size; j++) {
                if (arr[minimumIndex] > arr[j]) {
                    minimumIndex = j;
                }
            }
            if (minimumIndex != i) {
                int temp = arr[minimumIndex];
                arr[minimumIndex] = arr[i];
                arr[i] = temp;
            }
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for (int i = 1; i < size; i++) {
            int certainIndex = i;
            for (int j = i - 1; j >= 0; j--) {
                if (arr[certainIndex] < arr[j]) {
                    int temp = arr[certainIndex];
                    arr[certainIndex] = arr[j];
                    arr[j] = temp;
                    certainIndex = j;
                }else {
                    break;
                }
            }
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        if (size == 1) {
            return arr;
        }
        //dividing to 2 parts
        int[] arr1 = new int[size/2];
        int[] arr2 = new int[size - size/2];
        for (int i = 0; i < size/2; i++) {
            arr1[i] = arr[i];
        }
        for (int i = 0; i < (size -size/2); i++) {
            arr2[i] = arr[i + size/2];
        }
        arr1 = mergeSort(arr1, arr1.length);
        arr2 = mergeSort(arr2, arr2.length);
        //concatenating 2 parts
        int indexArr = 0,indexArr1 = 0, indexArr2 = 0;
        while (indexArr2 < arr2.length && indexArr1 < arr1.length){
            if (arr1[indexArr1] < arr2[indexArr2]) {
                arr[indexArr] = arr1[indexArr1];
                indexArr++;
                indexArr1++;
            }else {
                arr[indexArr] = arr2[indexArr2];
                indexArr2++;
                indexArr++;
            }
        }
        while (indexArr2 != arr2.length) {
            arr[indexArr] = arr2[indexArr2];
            indexArr++;
            indexArr2++;
        }
        while (indexArr1 != arr1.length) {
            arr[indexArr] = arr1[indexArr1];
            indexArr++;
            indexArr1++;
        }
        return arr;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int leftIndex = 0,rightIndex = arr.length - 1;
        while (leftIndex <= rightIndex) {
            int mid = (leftIndex + (rightIndex - 1))/2;
            if (value == arr[mid]){
                return mid;
            }
            if (value > arr[mid]) {
                leftIndex = mid + 1;
            }
            if (value < arr[mid]){
                rightIndex = mid - 1;
            }

        }
        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        return searchRecursively(arr,value,0, arr.length - 1);
    }
    public int searchRecursively(int[] arr, int value, int leftIndex, int rightIndex) {
        if (leftIndex <= rightIndex) {
            int mid = (leftIndex + (rightIndex - 1))/2;
            if (value == arr[mid]){
                return mid;
            }
            if (value > arr[mid]) {
                return searchRecursively(arr, value,  mid + 1, rightIndex);
            }
            if (value < arr[mid]){
                return searchRecursively(arr, value, leftIndex, mid - 1);
            }
        }
        return -1;
    }
}